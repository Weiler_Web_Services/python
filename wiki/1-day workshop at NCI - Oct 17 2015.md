# 1-day workshop hosted by [WITS Ireland](http://witsireland.com) at [NCI](https://www.ncirl.ie/) (Oct 17 2015)

Workshop content can be downloaded from [https://bitbucket.org/codinggrace/html5-css3-classes/downloads](https://bitbucket.org/codinggrace/html5-css3-classes/downloads).

* WITS-Workshop.zip

### Exercises

Aside from the exercises above, you can also find Part 3 examples (blog, normalise.css, blog post, floats and positioning) at [http://stephaniefrancis.github.io/codinggrace/](http://stephaniefrancis.github.io/codinggrace/)

#### Exercise1
Basic tags and structure of a HTML document.
####Exercise2
More HTML and introducing CSS and how to link to HTML document.
####Exercise3
Semantic HTML5, CSS design and linking to Google Fonts.

## Slides
* [Introduction slides](https://docs.google.com/presentation/d/1rmvhdYcHQ_wKwb_M3CLWZ7782Li_kQ2_WSHg95grHTM/pub?start=false&loop=false&delayms=3000) - Has announcements of upcoming events
* [Part 1 - What is HTML and CSS?](https://docs.google.com/presentation/d/1p6GNKoo0Ssm9IHYx0RBtSiLW6HvhZxICAL1yb74Pzy4/pub?start=false&loop=false&delayms=3000)
* [Part 2 - More about HTML5 and CSS3](https://docs.google.com/presentation/d/13vS4ZqSE0q0bq046W5hMm4pjWyy8fLV0ZODoCfIPpEI/pub?start=false&loop=false&delayms=3000)
