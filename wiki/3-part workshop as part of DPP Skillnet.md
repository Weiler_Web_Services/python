#  3-part workshop as part of DPP Skillnet

Workshop content can be downloaded from [https://bitbucket.org/codinggrace/html5-css3-classes/downloads](https://bitbucket.org/codinggrace/html5-css3-classes/downloads).

* ExerciseFinal.zip
* html5_css3_workshop.zip

### Exercises

Aside from the exercises above, you can also find Part 3 examples (blog, normalise.css, blog post, floats and positioning) at [http://stephaniefrancis.github.io/codinggrace/](http://stephaniefrancis.github.io/codinggrace/)

#### Exercise1
Basic tags and structure of a HTML document.

#### Exercise2
More HTML and introducing CSS and how to link to HTML document.

#### Exercise3
Semantic HTML5, CSS design and linking to Google Fonts.

#### ExerciseFinal
Create your own resume/cv web page.

* resume-text.txt - contains sample cv/resume details to include in a resume/cv HTML page
* SameResumeTemplate - folder contains sample HTML and CSS code for a resume page for your reference. (Template thanks to http://sampleresumetemplate.net)

## Slides
* [Part 1 - setup and intro to HTML5 and CSS3](http://bit.ly/1Gx2RIE)
* [Part 2 - More about HTML5 and CSS3](http://bit.ly/1Hnhtt5)
* [Part 3 - Debugging, layouts and positioning, and final exercise to create a cv/profile page](http://bit.ly/1f5y0XH)