# Workshop Content and Slides
* [3-part workshop as part of DPP Skillnet](https://bitbucket.org/codinggrace/html5-css3-classes/wiki/3-part%20workshop%20as%20part%20of%20DPP%20Skillnet)

* [1-day workshop at NCI - Sat Oct 17 2015](https://bitbucket.org/codinggrace/html5-css3-classes/wiki/1-day%20workshop%20at%20NCI%20-%20Oct%2017%202015)

---

# References

## General
* https://thimble.mozilla.org - Thimble is an online code editor that makes it easy to create and publish your own web pages while learning HTML, CSS & JavaScript
* http://caniuse.com - "Can I use" provides up-to-date browser support tables for support of front-end web technologies on desktop and mobile web browsers
* http://gs.statcounter.com - Free, online visitor stats tool

## HTML5 Resources
* http://www.html5rocks.com
* http://html5doctor.com
* http://reference.sitepoint.com/html
* http://woorkup.com/2009/12/16/html5-visual-cheat-sheet-reloaded/

## CSS3
* http://css3please.com
* http://css-tricks.com/almanac
* http://csszengarden.com - A demonstration of what can be accomplished through CSS-based design
* http://realworldcss3.com/resources
* http://reference.sitepoint.com/css
* http://simplilearn.com/css3-resources-ultimate-list-article
* https://necolas.github.io/normalize.css - Resetting CSS
* http://deepubalan.com/blog/2010/03/29/rgba-vs-opacity-the-difference-explained - RGBA vs Opacity

## Lorem Ipsum Generators / Generic Placeholders
* http://www.lipsum.com
* http://www.catipsum.com
* http://robohash.org
* http://placekitten.com

## Frameworks
* http://getbootstrap.com

## Web fonts
* https://www.google.com/fonts (free)
* https://fontlibrary.org (Open-sourced, free)
* https://typekit.com/plans (limited fonts for free use, otherwise paid service)