#!pyton3

# How to create a multiple directory with pathlib in Python

import os
import pathlib

p = pathlib.Path('1/2/3/4/5/6/7/8/9/10')
p.mkdir(parents=True)
