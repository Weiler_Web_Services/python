#!python

# rocketship you can control using the arrow keys

html {
  margin: 0;
  padding: 0;
  background: url("space.jpg") no-repeat;
  width: 400px;
  height: 400px;
  overflow: hidden;
}


body {
  margin: 0;
  padding: 0;
  overflow: hidden;
  width: 100%;
  height: 100%;
}

#rocketship {
  position: relative;
}
