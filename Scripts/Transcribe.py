# Transcribe

'''
Audio transcription works by a few steps:
mp3 to wav conversion,
loading the audio file,
feeding the audio file to a speceh recongition system.
'''

import speech_recognition as sr
from os import path
from pydub import AudioSegment

# convert mp3 file to wav                                                       
sound = AudioSegment.from_mp3("Sweet Sweet Spirit With Lyrics.mp3")
sound.export("Sweet Sweet Spirit With Lyrics.wav", format="wav")


# transcribe audio file                                                         
AUDIO_FILE = "Sweet Sweet Spirit With Lyrics.wav"

# use the audio file as the audio source                                        
r = sr.Recognizer()
with sr.AudioFile(AUDIO_FILE) as source:
        audio = r.record(source)  # read the entire audio file                  

        print("Transcription: " + r.recognize_google(audio))
