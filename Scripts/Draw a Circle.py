import turtle

# our turtle is named tina
tina = turtle.Turtle()
tina.shape("turtle")

# tina can create a blue circle
tina.color("blue")
tina.begin_fill()
tina.circle(75)
tina.end_fill()

# put the cursor after tina and enter a dot (.) to see what else she understands!
tina