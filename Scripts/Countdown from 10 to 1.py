#!python

# Countdown from 10 to 1

import time

for i in range(10, 0, -1):
  print i
  time.sleep(1)
