#!/usr/bin/env python
#tt.py -- tetris game

import sys
import time
import pygame
from pygame import font
from random import choice
from colorama import init, AnsiToWin32, Fore

init(wrap=False)
stream = AnsiToWin32(sys.stderr).stream

""" game piece's """

#     0  #
#   000  #   
J = [[[0, 1, 0], [0, 1, 0], [0, 1, 1]],
     [[0, 0, 1], [1, 1, 1], [0, 0, 0]],
     [[1, 1, 0], [0, 1, 0], [0, 1, 0]],
     [[0, 0, 0], [1, 1, 1], [1, 0, 0]]]

#    00  #
#   00   #
S = [[[0, 1, 1], [1, 1, 0], [0, 0, 0]],
     [[1, 0, 0], [1, 1, 0], [0, 1, 0]],
     [[0, 0, 0], [0, 1, 1], [1, 1, 0]],
     [[0, 1, 0], [0, 1, 1], [0, 0, 1]]]

#   00   #
#    00  #
Z = [[[1, 1, 0], [0, 1, 1], [0, 0, 0]],
     [[0, 1, 0], [1, 1, 0], [1, 0, 0]],
     [[0, 0, 0], [1, 1, 0], [0, 1, 1]],
     [[0, 1, 0], [0, 1, 1], [0, 0, 1]]]

#    0   #
#   000  #
T = [[[0, 1, 0], [1, 1, 1], [0, 0, 0]],
     [[0, 1, 0], [1, 1, 0], [0, 1, 0]],
     [[0, 0, 0], [1, 1, 1], [0, 1, 0]],
     [[0, 1, 0], [0, 1, 1], [0, 1, 0]]]

#   0    #
#   000  #
L = [[[0, 1, 0], [0, 1, 0], [1, 1, 0]],
     [[0, 0, 0], [1, 1, 1], [0, 0, 1]],
     [[0, 1, 1], [0, 1, 0], [0, 1, 0]],
     [[1, 0, 0], [1, 1, 1], [0, 0, 0]]]

#   0000  #
I = [[[0, 1, 0,], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]],
     [[0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]]

#   00  #
#   00  #
O = [[[0, 1, 1], [0, 1, 1]]]

tetrominoes = [I, J, L, O, S, T, Z]
tetri_colors = [(0,240,240), (0,0,216), (240,160,0), (240,240,0), (0,240,0), (160,0,240), (240,0,0)]

class Piece():
    """Tetromino Piece Object"""
    
    def __init__(self, screen, start=(10, 0)):
        self.piece = choice(tetrominoes)
        self.next_piece = choice(tetrominoes)
        self.color = tetri_colors[tetrominoes.index(self.piece)]
        self.state = 0
        self.turn = 0
        self.locked_pos = {}
        self.start = start
        self.grid = {(i, j): grid_color for i in range(play_width//cube_size)
                     for j in range(play_height//cube_size)}
        self.score = 0

    def grids(self):
        """Create grid with shapes"""
        self.coor()
        self.grid = {(i, j): grid_color for i in range(16) for j in range(24)}
        for pos, color in self.locked_pos.items():
            self.grid[pos] = color
        for colour, pos_list in self.coordinates.items():
            for pos in pos_list:
                if pos in self.grid and pos not in self.locked_pos.keys():
                    self.grid[pos] = colour

    def coor(self):
        """Store position of cubes under they respective parent color"""
        self.coordinates = {self.color: [(i + self.start[0] - 3, j + self.start[1] - 4)
                            for j in range(len(self.piece[self.state])) 
                            for i in range(len(self.piece[self.state][j])) 
                            if self.piece[self.state][j][i] == 1]}

    def check_events(self, event):
        """Check for events on keyboard and change shape respectively"""
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                self.start = (self.start[0] + 1, self.start[1])
                self.coor()
                for pos in self.coordinates[self.color]:
                    if pos not in self.grid and pos[1] >= 0 or pos in self.locked_pos.keys():
                        self.start = (self.start[0] - 1, self.start[1])
                        self.coor()
                        break
                
            elif event.key == pygame.K_LEFT:
                self.start = (self.start[0] - 1, self.start[1])
                self.coor()
                for pos in self.coordinates[self.color]:
                    if (pos not in self.grid and pos[1] >= 0) or pos in self.locked_pos.keys():
                        self.start = (self.start[0] + 1, self.start[1])
                        self.coor()
                        break


            elif event.key == pygame.K_DOWN:
                self.start = (self.start[0], self.start[1] + 1)
                self.coor()
                for pos in self.coordinates[self.color]:
                    if (pos not in self.grid and pos[1] >= 0) or pos in self.locked_pos.keys():
                        self.start = (self.start[0], self.start[1] - 1)
                        self.coor()
                        break
    

            elif event.key == pygame.K_UP:
                self.turn += 1
                self.state =  self.turn % len(self.piece)
                self.coor()
                for pos in self.coordinates[self.color]:
                    if (pos not in self.grid and pos[1] >= 0) or pos in self.locked_pos.keys():
                        self.turn -= 1
                        self.state =  self.turn % len(self.piece)
                        self.coor()
                        break
                
    def move(self):
        """Move our shape down"""
        self.check_lost()
        self.start = (self.start[0], self.start[1] + 1)
        self.coor()
        for pos in self.coordinates[self.color]:
            if (pos not in self.grid and pos[1] >= 0) or pos in self.locked_pos.keys():
                self.start = (self.start[0], self.start[1] - 1)
                self.coor()
                self.locked_pos.update({pos: self.color for pos in self.coordinates[self.color] 
                                       if pos[1] >= 0})
                self.clear_rows()
                self.reset()
                break

    def check_lost(self):
        """Check if shape above the play box"""
        for pos in self.locked_pos.keys():
            if pos[1] <= 0:
                time.sleep(2)
                sys.exit()

    def clear_rows(self):
        """Check if cubes align in a row and clear them"""
        termination = []
        grid = [[(i,j) for i in range(play_width//cube_size)] for j in range(play_height//cube_size)]
        for i, row in enumerate(grid):
            for pos in row:
                if pos not in self.locked_pos:
                    termination = []
                    break
                termination.append(pos)                    

            if termination:
                self.score += 1
                for pos in termination:
                    self.locked_pos.pop(pos)
                lock_pos = [(l_p[0], l_p[1] + 1) for l_p in list(self.locked_pos.keys()) if l_p[1] < i]
                for pos in sorted(lock_pos, key=lambda x: x[1], reverse=True):
                    self.locked_pos[pos] = self.locked_pos[(pos[0], pos[1] - 1)]
                    self.locked_pos.__delitem__((pos[0], pos[1] - 1))
            termination = []


    def reset(self):
        """Reset all attributes"""
        self.piece = self.next_piece
        self.next_piece = choice(tetrominoes)
        self.color = tetri_colors[tetrominoes.index(self.piece)]
        self.state = 0
        self.turn = 0
        self.start = (10, 0)


def draw_grid():
    """Draw grid lines"""
    for i in range(1, play_width//cube_size):
        startx = (screen_width - play_width) / 2 + (i * cube_size)
        starty = (screen_height - play_height) / 2
        endx = (screen_width - play_width) / 2 + (i * cube_size)
        endy = play_height + (screen_height - play_height) / 2
        pygame.draw.line(screen, line_color, (startx, starty), (endx, endy), 1)

    for j in range(1, play_height//cube_size):
        startx = (screen_width - play_width) / 2
        starty = (screen_height - play_height) / 2 + (j * cube_size)
        endx = play_width + (screen_width - play_width) / 2
        endy = (screen_height - play_height) / 2 + (j * cube_size)
        pygame.draw.line(screen, line_color, (startx, starty), (endx, endy), 1)

def fill_grid(grid):
    """Fill grid squares with respective colors"""   
    for pos, color in grid.items():
        rect = pygame.Rect((screen_width - play_width)/2 + (pos[0] * cube_size),
                    (screen_height - play_height)/2 + (pos[1] * cube_size), cube_size, cube_size)
        pygame.draw.rect(screen, color, rect)

    draw_grid()

def draw_play_window(grid):
    """Draw play box"""
    l_font = font.SysFont("comicsans", 60)
    label = l_font.render("TETRIS", 1, (252, 15, 204))
    win_rect = pygame.Rect((screen_width - play_width) / 2 - 3, (screen_height - play_height) / 2 - 3,
                           play_width + 6, play_height + 6)
    pygame.draw.rect(screen, (212, 175, 55), win_rect, 4)
    fill_grid(grid)
    screen.blit(label, (screen_width / 2 - label.get_width() / 2, 30))

def next_shape(piece):
    """Display next shape next to the play box"""
    n_font = font.SysFont("comicsans", 30)
    label = n_font.render("Next Piece: ", 1, (225, 200, 215))
    screen.blit(label, (play_width + (screen_width - play_width)//2 + cube_size, screen_height - play_height / 1.2))
    shape = piece[0]
    for j, row in enumerate(shape):
        for i, cube in enumerate(row):
            if cube == 1:
                rect = pygame.Rect(play_width + (screen_width - play_width)//2 + (i * cube_size) + cube_size,
                                   (screen_height - play_height / 1.2) + 50 + (j * cube_size),
                                    cube_size, cube_size)
                pygame.draw.rect(screen, tetri_colors[tetrominoes.index(piece)], rect)

def score(score):
    """Get high score and score then display them"""
    filename = "highscore.txt"
    with open(filename, "r") as f_obj:
        content = f_obj.read()
    if int(content) < (score * 10):
        with open(filename, "w") as f_obj:
            f_obj.write(str(score * 10))
        highscore = score
    else:
        highscore = content

    s_font = font.SysFont("comicsans", 30)
    label = s_font.render(f"Score: {score * 10}", 1, ((225, 200, 215)))
    screen.blit(label, (play_width + (screen_width - play_width)//2 + cube_size, screen_height - play_height / 1))
    label = s_font.render(f"HighScore: {highscore}", 1, ((225, 200, 215)))
    screen.blit(label, (play_width + (screen_width - play_width)//2 - cube_size, screen_height - play_height / 0.8))

def io_screen(info):
    """Intro and outro screens"""
    pygame.init()
    screen_width = 700
    screen_height = 800
    play_width = 400
    play_height = 600
    screen = pygame.display.set_mode((screen_width, screen_height))
    s_font = font.SysFont("comicsans", 93)
    label = s_font.render(info, 1, (252, 15, 204))
    run = True

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if info.startswith("P"):
                if event.type == pygame.KEYDOWN:
                    run = False
            elif info.startswith("G"):
                time.sleep(2)
                run = False
        screen.fill((0, 0, 26))
        screen.blit(label, (0 + (screen_width - label.get_width())//2, screen_height//2 - label.get_height()//2))
        pygame.display.update()

def main():
    """Main game function"""
    global screen_width, screen_height, play_width, play_height, cube_size, line_color, grid_color, screen
    pygame.init()

    screen_width = 700
    screen_height = 800
    play_width = 400
    play_height = 600
    cube_size = 25
    screen_color = (0, 0, 12)
    line_color  = (212, 150, 50)
    grid_color = (0, 0, 26)
    screen = pygame.display.set_mode((screen_width, screen_height))
    pygame.display.set_caption("TETRIS")

    piece = Piece(screen)
    next_piece = Piece(screen)
    fall_time = 0
    fall_speed = 0.20
    clock = pygame.time.Clock()
    run = True
    
    while run:
        fall_time += clock.get_rawtime()
        if fall_time/1000 > fall_speed:
            piece.move()
            fall_time = 0
        clock.tick()

        screen.fill(screen_color)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            piece.check_events(event)
        piece.grids()
        draw_play_window(piece.grid)
        next_shape(piece.next_piece)
        score(piece.score)
        pygame.display.update()

if __name__ == "__main__":
    try:
        io_screen("Press Any Key to Play")
        main()
    except:
        io_screen("Good Bye!")
        sys.exit()