import pygame
import random
import time

pygame.init()

win = pygame.display.set_mode((1536, 288))

pygame.display.set_caption("Welcome to the Overlords RPG!")

screenWidth = 1536


class Player(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 8
        self.jumpCount = 7
        self.j_land = -7
        self.walkCount = 0
        self.standing = True
        self.a_bow = False
        self.a_attack = False
        self.isJump = False
        self.left = False
        self.right = True
        self.hitBox = (self.x + 15, self.y + 7, 20, 30)
        self.health = 100

    def draw(self, win):

        if self.walkCount + 1 >= 18:
            self.walkCount = 0

        if self.standing:
            if self.right:
                if self.a_bow:
                    win.blit(ani_bow[self.walkCount//3], (self.x, self.y))
                    self.walkCount += 1
                elif self.a_attack:
                    win.blit(s_attack[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
                elif self.right:
                    win.blit(char[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1

            elif self.left:
                if self.a_bow:
                    win.blit(ani_bow_2[self.walkCount//3], (self.x, self.y))
                    self.walkCount += 1
                elif self.a_attack:
                    win.blit(s_attack2[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
                elif self.left:
                    win.blit(char_2[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1

        elif not self.standing:
            if self.right:
                if self.a_bow:
                    win.blit(ani_bow[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
                elif self.a_attack:
                    win.blit(s_attack[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
                elif self.right:
                    win.blit(walkRight[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
            elif self.left:
                if self.a_bow:
                    win.blit(ani_bow_2[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
                elif self.a_attack:
                    win.blit(s_attack2[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
                elif self.left:
                    win.blit(walkLeft[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
        pygame.draw.rect(win, (255, 0, 0), (55, 25, 100, 10))
        pygame.draw.rect(win, (0, 255, 0), (55, 25, self.health, 10))
        self.hitBox = (self.x + 15, self.y + 7, 20, 30)
        #pygame.draw.rect(win, (255, 0, 0), self.hitBox, 2)

    def hit(self):
        if self.health > 0:
            self.health -= Skeleton.damage
        else:
            Skeleton.attack = False
            death_txt = pygame.font.SysFont('Sans', 100)
            text = death_txt.render('YOU DIED!', 1, (255, 0, 0))
            win.blit(text, (768, 144))
            # deathSound.play()
            pygame.display.update()
            i = 0
            while i < 300:
                pygame.time.delay(10)
                i += 1
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        i = 301
                        pygame.quit()
            Chosen.health = 100
            game_intro()

        if Skeleton.left:
            self.x -= 50

        elif Skeleton.right:
            self.x += 50

        if not self.isJump:
            self.isJump = True
            self.walkCount += 1

        '''else:
            if self.jumpCount_hit >= self.j_land_hit:
                neg = 1
                if self.jumpCount_hit < 0:
                    neg = -1
                self.y -= (self.jumpCount_hit ** 2) * 0.5 * neg
                self.jumpCount_hit -= 1
            else:
                self.isJump = False
                self.jumpCount_hit = 2'''


class Enemy(object):
    walkRight = [pygame.image.load("swr000.png"), pygame.image.load("swr001.png"), pygame.image.load("swr002.png"), pygame.image.load("swr003.png"), pygame.image.load("swr004.png"), pygame.image.load("swr005.png"), pygame.image.load("swr006.png"), pygame.image.load("swr007.png"), pygame.image.load("swr008.png"), pygame.image.load("swr009.png"), pygame.image.load("swr010.png"), pygame.image.load("swr011.png"), pygame.image.load("swr012.png")]
    walkLeft = [pygame.image.load("swl000.png"), pygame.image.load("swl001.png"), pygame.image.load("swl002.png"), pygame.image.load("swl003.png"), pygame.image.load("swl004.png"), pygame.image.load("swl005.png"), pygame.image.load("swl006.png"), pygame.image.load("swl007.png"), pygame.image.load("swl008.png"), pygame.image.load("swl009.png"), pygame.image.load("swl010.png"), pygame.image.load("swl011.png"), pygame.image.load("swl012.png")]
    m_attackRight = [pygame.image.load("smar007.png"), pygame.image.load("smar008.png"), pygame.image.load("smar009.png"), pygame.image.load("smar010.png"), pygame.image.load("smar011.png"), pygame.image.load("smar012.png"), pygame.image.load("smar013.png"), pygame.image.load("smar014.png"), pygame.image.load("smar015.png"), pygame.image.load("smar016.png"), pygame.image.load("smar017.png")]
    m_attackLeft = [pygame.image.load("smal007.png"), pygame.image.load("smal008.png"),
                     pygame.image.load("smal009.png"), pygame.image.load("smal010.png"),
                     pygame.image.load("smal011.png"), pygame.image.load("smal012.png"),
                     pygame.image.load("smal013.png"), pygame.image.load("smal014.png"),
                     pygame.image.load("smal015.png"), pygame.image.load("smal016.png"),
                     pygame.image.load("smal017.png")]

    def __init__(self, x, y, width, height, end):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.end = end
        self.path = [Chosen.x, self.x]
        self.walkCount = 0
        self.vel = 5
        self.attack = False
        self.right = False
        self.left = True
        self.isJump = False
        self.jumpCount = 10
        self.j_land = -10
        self.hitBox = (self.x, self.y + 9, 17, 27)
        self.health = 10
        self.max_health = 10
        self.visible = True
        self.drop = False
        self.damage = 10


    def draw(self, win):
        self.move()
        if self.visible:
            if self.walkCount + 1 >= 33:
                self.walkCount = 0

            if self.right:
                if self.attack:
                    win.blit(self.m_attackRight[self.walkCount//3], (self.x, self.y))
                    self.walkCount += 1
                elif self.right:
                    win.blit(self.walkRight[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
            elif self.left:
                if self.attack:
                    win.blit(self.m_attackLeft[self.walkCount // 3], (self.x, self.y))
                    self.walkCount += 1
                elif self.left:
                    win.blit(self.walkLeft[self.walkCount//3], (self.x, self.y))
                    self.walkCount += 1

            pygame.draw.rect(win, (255, 0, 0), (self.hitBox[0] - 5, self.hitBox[1] - 10, self.max_health, 5))
            pygame.draw.rect(win, (0, 255, 0), (self.hitBox[0] - 5, self.hitBox[1] - 10, self.health, 5))
            self.hitBox = (self.x + 5, self.y + 5, 17, 30)
        #pygame.draw.rect(win, (255, 0, 0), self.hitBox, 2)

    def move(self):
        if self.vel >= 0:
            self.right = True
            self.left = False

            if self.x + self.vel <= self.path[1]:
                self.x += self.vel
                self.path[1] = Chosen.x
            else:
                self.vel = self.vel * -1
                self.walkCount = 0
        else:
            self.left = True
            self.right = False

            if self.x - self.vel >= self.path[0]:
                self.x += self.vel
                self.path[0] = Chosen.x
            else:
                self.vel = self.vel * -1
                self.walkCount = 0




    def hit(self):
        hitSound.play()

        if Skeleton.health <= 0:
            self.hitBox = (0, 0, 0, 0)
            self.visible = False
            self.drop = True

        if self.left:
            self.x += 20

        elif self.right:
            self.x -= 20

        if not self.isJump:
            self.isJump = True
            self.walkCount += 1
        else:
            if self.jumpCount >= self.j_land:
                if self.jumpCount < 0:
                    self.y -= (self.jumpCount ** 2) * 0.5
                    self.jumpCount -= 1
            else:
                self.isJump = False
                self.jumpCount = 10

        print("Hit!")



class Inventory(object):

    def __init__(self, objects, slots, gold):
        self.gold = 0
        self.objects = []
        self.slots = 20

    def draw(self, win):
        font = pygame.font.SysFont("Sans", 30, True, True)
        txt = font.render('Items:{}'.format(len(self.objects)), 1, (0, 0, 255))
        win.blit(txt, (1200, 10))



class Item(object):
    bone_i = [pygame.image.load("Bone.png")]

    def __init__(self, x, y):
        self.x = x
        self.y = y
        #self.width = width
        #self.height = height
        self.visible = True

    def draw(self, win):
        win.blit(self.bone_i[0], (self.x, self.y))
                #if self.bone_i.y < 217:
                    #self.bone_i.y += 1



class Projectile(object):
    def __init__(self, x, y, facing):
        self.x = x
        self.y = y
        self.facing = facing
        self.vel = 9 * facing
        self.damage = 2

    def draw(self, win):
        win.blit(pro_tile_r, (self.x, self.y))


class Swing(object):
    def __init__(self, x, y, facing):
        self.x = x
        self.y = y
        self.right = False
        self.left = False
        self.walkCount = 0
        self.facing = facing
        self.vel = 8 * facing
        self.damage = 5

    def draw(self, win):
        if self.walkCount + 1 >= 18:
            self.walkCount = 0
        if Chosen.right and self.x >= Chosen.x:
            win.blit(pro_swing[self.walkCount//3], (self.x, self.y))
            self.walkCount += 1
        if Chosen.left and self.x - 30 <= Chosen.x:
            win.blit(pro_swing_L[self.walkCount//3], (self.x, self.y))
            self.walkCount += 1

class Start(object):

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self. width = width
        self.height = height
        self.visible = True

    def draw(self, win):
        if self.visible:
            start = pygame.image.load("Start.jpg").convert()
            start2 = pygame.transform.scale(start, (100, 50))
            win.blit(start2, (450, 150))
            self.hitbox = (self.x + 15, self.y + 7, 100, 50)


class Exit(object):

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.visible = True

    def draw(self, win):
        if self.visible:
            exit = pygame.image.load("Exit.jpg").convert()
            exit2 = pygame.transform.scale(exit, (100, 50))
            win.blit(exit2, (850, 150))
            self.hitbox = (self.x + 15, self.y + 7, 100, 50)


# Character animations
walkRight = [pygame.image.load('adventurer-run3-00.png'), pygame.image.load('adventurer-run3-01.png'),
             pygame.image.load('adventurer-run3-02.png'), pygame.image.load('adventurer-run3-03.png'),
             pygame.image.load('adventurer-run3-04.png'), pygame.image.load('adventurer-run3-05.png')]
walkLeft = [pygame.image.load('adventurer-run3-00-L.png'), pygame.image.load('adventurer-run3-01-L.png'),
            pygame.image.load('adventurer-run3-02-L.png'), pygame.image.load('adventurer-run3-03-L.png'),
            pygame.image.load('adventurer-run3-04-L.png'), pygame.image.load('adventurer-run3-05-L.png')]
bg = pygame.image.load('environment-preview.jpg').convert()
char = [pygame.image.load('adventurer-idle-2-00.png'), pygame.image.load('adventurer-idle-2-01.png'),
        pygame.image.load('adventurer-idle-2-02.png'), pygame.image.load('adventurer-idle-2-00.png'),
        pygame.image.load('adventurer-idle-2-01.png'), pygame.image.load('adventurer-idle-2-02.png')]
char_2 = [pygame.image.load('adventurer-idle-2-00-L.png'), pygame.image.load('adventurer-idle-2-01-L.png'), pygame.image.load('adventurer-idle-2-00-L.png'), pygame.image.load('adventurer-idle-2-01-L.png'), pygame.image.load('adventurer-idle-2-00-L.png'), pygame.image.load('adventurer-idle-2-01-L.png')]
j_air = [pygame.image.load('adventurer-jump-00.png')]
j_fall = [pygame.image.load('adventurer-fall-01.png')]

# Attack animations
ani_bow = [pygame.image.load('adventurer-bow-07.png'), pygame.image.load('adventurer-bow-07.png'), pygame.image.load('adventurer-bow-07.png'), pygame.image.load('adventurer-bow-07.png'), pygame.image.load('adventurer-bow-07.png'), pygame.image.load('adventurer-bow-07.png')]
ani_bow_2 = [pygame.image.load('adventurer-bow-07-L.png'), pygame.image.load('adventurer-bow-07-L.png'), pygame.image.load('adventurer-bow-07-L.png'), pygame.image.load('adventurer-bow-07-L.png'), pygame.image.load('adventurer-bow-07-L.png'), pygame.image.load('adventurer-bow-07-L.png')]
s_attack = [pygame.image.load('adventurer-attack3-02.png'), pygame.image.load('adventurer-attack3-02.png'), pygame.image.load('adventurer-attack3-02.png'), pygame.image.load('adventurer-attack3-02.png'), pygame.image.load('adventurer-attack3-02.png'), pygame.image.load('adventurer-attack3-02.png')]
s_attack2 = [pygame.image.load('adventurer-attack3-02-L.png'), pygame.image.load('adventurer-attack3-02-L.png'), pygame.image.load('adventurer-attack3-02-L.png'), pygame.image.load('adventurer-attack3-02-L.png'), pygame.image.load('adventurer-attack3-02-L.png'), pygame.image.load('adventurer-attack3-02-L.png')]
pro_swing = [pygame.image.load('effect_attack.png'), pygame.image.load('effect_attack.png'), pygame.image.load('effect_attack.png'), pygame.image.load('effect_attack.png'), pygame.image.load('effect_attack.png'), pygame.image.load('effect_attack.png')]
pro_swing_L = [pygame.image.load('effect_attack_2.png'), pygame.image.load('effect_attack_2.png'), pygame.image.load('effect_attack_2.png'), pygame.image.load('effect_attack_2.png'), pygame.image.load('effect_attack_2.png'), pygame.image.load('effect_attack_2.png')]
pro_tile = pygame.image.load('trail_00.png').convert()
pro_tile_r = pygame.transform.scale(pro_tile, (10, 5))


# Sound effects
hitSound = pygame.mixer.Sound('Skeleton Roar.wav')
bulletSound = pygame.mixer.Sound('Arrow.wav')
swordSound = pygame.mixer.Sound('Sword2.wav')
#deathSound = pygame.mixer.Sound('death_sound.wav')

# Background Music
music = pygame.mixer.music.load('Risen.wav')
pygame.mixer.music.play(-1)

# Frame rate
clock = pygame.time.Clock()

# Globals
start_intro = Start(450, 150, 100, 50)
exit_intro = Exit(850, 150, 100, 50)
font = pygame.font.SysFont("Sans", 30, True, True)
Chosen = Player(0, 215, 50, 37)
inventory = Inventory(0, 20, 0)
m_spawn = random.randrange(400, 700, 50)
Skeleton = Enemy(m_spawn, 217, 22, 33, 1150)
bone = Item(500, 217)
bullets = []
attacks = []
monsters = []
num_monsters = 3
monsters_on_screen = 0
items = []


def redraw_game_window():

    # Base screen UI
    win.blit(bg, (0, 0))
    txt = font.render('Hp: ', 1, (0, 0, 255))
    hp_txt = font.render("{}".format(Chosen.health), 1, (200, 50, 50))
    win.blit(hp_txt, (65, 35))
    win.blit(txt, (0, 10))

    # Character and Monsters
    Chosen.draw(win)
    inventory.draw(win)
    Skeleton.draw(win)

    # Items
    for bone in items:
        bone.draw(win)
    for bullet in bullets:
        bullet.draw(win)
    for attack in attacks:
        attack.draw(win)
    pygame.display.update()

def redraw_game_window_intro():

    # Base screen UI
    win.blit(bg, (0, 0))

    # Character and Monsters
    Chosen.draw(win)
    start_intro.draw(win)
    exit_intro.draw(win)

    # Items
    for bullet in bullets:
        bullet.draw(win)
    for attack in attacks:
        attack.draw(win)
    pygame.display.update()



def game_intro():

    attack_cool = 0
    if attack_cool > 0:
        attack_cool += 1
    if attack_cool > 20:
        attack_cool = 0

    intro = True
    while intro:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        for attack in attacks:
            if Chosen.x + 75 > attack.x and attack.x > Chosen.x - 75 and attack.x > 0:
                attack.x += attack.vel
                Chosen.a_attack = False
            else:
                if len(attacks) == 0:
                    print("missed!")
                elif len(attacks) > 0:
                    attacks.pop(attacks.index(attack))

        for bullet in bullets:
            if bullet.y < start_intro.hitbox[1] + start_intro.hitbox[3] and bullet.y > start_intro.hitbox[1]:
                if bullet.x > start_intro.hitbox[0] and bullet.x < start_intro.hitbox[0] + start_intro.hitbox[2] :
                    bullets.pop(bullets.index(bullet))
                    game_loop()
                    pygame.quit()

            if bullet.y < exit_intro.hitbox[1] + exit_intro.hitbox[3] and bullet.y > exit_intro.hitbox[1]:
                if bullet.x > exit_intro.hitbox[0] and bullet.x < exit_intro.hitbox[0] + exit_intro.hitbox[2] :
                    pygame.quit()

            if Chosen.x + 100 > bullet.x > Chosen.x - 100 and bullet.x > 0:
                Chosen.a_bow = False

            if bullet.x < 1500 and bullet.x > 0:
                bullet.x += bullet.vel
            else:
                bullets.pop(bullets.index(bullet))

        keys = pygame.key.get_pressed()

        # BOW ATTACK
        if keys[pygame.K_SPACE] and attack_cool == 0:
            bulletSound.play()
            Chosen.a_bow = True
            if Chosen.left:
                facing = -1
            else:
                facing = 1
            if len(bullets) < 50:
                bullets.append(
                    Projectile(round(Chosen.x + Chosen.width // 2), round(Chosen.y + Chosen.height // 2), facing))
                attack_cool = 1
            elif len(bullets) >= 50:
                Chosen.a_bow = False


        # SWORD ATTACK
        if keys[pygame.K_z] and attack_cool == 0:
            Chosen.a_attack = True
            if Chosen.left:
                facing = -1
            else:
                facing = 1
            if len(attacks) < 1:
                swordSound.play()
                attacks.append(Swing(round(Chosen.x + Chosen.width // 2), round(Chosen.y + Chosen.height // 2), facing))
                attack_cool = 1
            else:
                Chosen.a_attack = False

        # MOVE LEFT
        if keys[pygame.K_LEFT] and Chosen.x > Chosen.vel:
            Chosen.x -= Chosen.vel
            Chosen.left = True
            Chosen.right = False
            Chosen.standing = False

        # MOVE RIGHT
        elif keys[pygame.K_RIGHT] and Chosen.x < screenWidth - Chosen.width - Chosen.vel:
            Chosen.x += Chosen.vel
            Chosen.right = True
            Chosen.left = False
            Chosen.standing = False
        else:
            Chosen.standing = True

        # JUMP
        if not Chosen.isJump:
            if keys[pygame.K_UP]:
                Chosen.isJump = True
                Chosen.walkCount += 1
        else:
            if Chosen.jumpCount >= Chosen.j_land:
                neg = 1
                if Chosen.jumpCount < 0:
                    neg = -1
                Chosen.y -= (Chosen.jumpCount ** 2) * 0.5 * neg
                Chosen.jumpCount -= 1
            else:
                Chosen.isJump = False
                Chosen.jumpCount = 7

        redraw_game_window_intro()
        clock.tick(27)


# Main Loop
def game_loop():


    spawn_time = 0
    attack_cool = 0
    run = True
    while run:

        clock.tick(27)

        if spawn_time > 0:
            spawn_time += 1
        if spawn_time > 100:
            spawn_time = 0


        if Chosen.hitBox[1] < Skeleton.hitBox[1] + Skeleton.hitBox[3] and Chosen.hitBox[1] + Chosen.hitBox[3] > \
                Skeleton.hitBox[1]:
            if Chosen.hitBox[0] + Chosen.hitBox[2] > Skeleton.hitBox[0] and Chosen.hitBox[0] < Skeleton.hitBox[0] + \
                    Skeleton.hitBox[2]:
                if Skeleton.left:
                    Skeleton.x += 20

                elif Skeleton.right:
                    Skeleton.x -= 20
                Skeleton.walkCount += 1
                Skeleton.attack = True
                Chosen.hit()
        else:
            Skeleton.attack = False


        if attack_cool > 0:
            attack_cool += 1
        if attack_cool > 20:
            attack_cool = 0

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        if Skeleton.drop:
            if len(items) < 1:
                items.append(Item((Skeleton.x + Skeleton.width//2), Skeleton.y + Skeleton.height//2))
                Skeleton.drop = False

        # bullet: Projectile
        for bullet in bullets:
            if bullet.y < Skeleton.hitBox[1] + Skeleton.hitBox[3] and bullet.y > Skeleton.hitBox[1]:
                if bullet.x > Skeleton.hitBox[0] and bullet.x < Skeleton.hitBox[0] + Skeleton.hitBox[2] :
                    Skeleton.hit()
                    Skeleton.health -= 2
                    bullets.pop(bullets.index(bullet))

            if Chosen.x + 100 > bullet.x > Chosen.x - 100 and bullet.x > 0:
                Chosen.a_bow = False

            if bullet.x < 1500 and bullet.x > 0:
                bullet.x += bullet.vel
            else:
                bullets.pop(bullets.index(bullet))

        for attack in attacks:
            if attack.y < Skeleton.hitBox[1] + Skeleton.hitBox[3] and attack.y > Skeleton.hitBox[1]:
                if attack.x > Skeleton.hitBox[0] and attack.x < Skeleton.hitBox[0] + Skeleton.hitBox[2] :
                    Skeleton.hit()
                    Skeleton.health -= 5
                    attacks.pop(attacks.index(attack))
                elif len(attacks) <= 0:
                    print("missed!")

            if Chosen.x + 75 > attack.x and attack.x > Chosen.x - 75 and attack.x > 0:
                attack.x += attack.vel
                Chosen.a_attack = False
            else:
                if len(attacks) == 0:
                    print("missed!")
                elif len(attacks) > 0:
                    attacks.pop(attacks.index(attack))


        keys = pygame.key.get_pressed()

        # BOW ATTACK
        if keys[pygame.K_SPACE] and attack_cool == 0:
            bulletSound.play()
            Chosen.a_bow = True
            if Chosen.left:
                facing = -1
            else:
                facing = 1
            if len(bullets) < 50:
                bullets.append(
                    Projectile(round(Chosen.x + Chosen.width // 2), round(Chosen.y + Chosen.height // 2), facing))
                attack_cool = 1
            elif len(bullets) >= 50:
                Chosen.a_bow = False

        # SWORD ATTACK
        if keys[pygame.K_z] and attack_cool == 0:
            Chosen.a_attack = True
            if Chosen.left:
                facing = -1
            else:
                facing = 1
            if len(attacks) < 1:
                swordSound.play()
                attacks.append(Swing(round(Chosen.x + Chosen.width // 2), round(Chosen.y + Chosen.height // 2), facing))
                attack_cool = 1
            else:
                Chosen.a_attack = False

        # MOVE LEFT
        if keys[pygame.K_LEFT] and Chosen.x > Chosen.vel:
            Chosen.x -= Chosen.vel
            Chosen.left = True
            Chosen.right = False
            Chosen.standing = False

        # MOVE RIGHT
        elif keys[pygame.K_RIGHT] and Chosen.x < screenWidth - Chosen.width - Chosen.vel:
            Chosen.x += Chosen.vel
            Chosen.right = True
            Chosen.left = False
            Chosen.standing = False
        else:
            Chosen.standing = True

        # PICK UP KEY
        if keys[pygame.K_x]:
            for bone in items:
                if Chosen.hitBox[1] < bone.x and Chosen.hitBox[1] + Chosen.hitBox[3] < bone.x:
                    if Chosen.hitBox[0] + Chosen.hitBox[2] > bone.x and Chosen.hitBox[0] < bone.x:
                        print(items[0])
                        items.pop(items.index(bone))
                        inventory.objects.append(bone)
        # JUMP
        if not Chosen.isJump:
            if keys[pygame.K_UP]:
                Chosen.isJump = True
                Chosen.walkCount += 1
        else:
            if Chosen.jumpCount >= Chosen.j_land:
                neg = 1
                if Chosen.jumpCount < 0:
                    neg = -1
                Chosen.y -= (Chosen.jumpCount ** 2) * 0.5 * neg
                Chosen.jumpCount -= 1
            else:
                Chosen.isJump = False
                Chosen.jumpCount = 7

        if keys[pygame.K_ESCAPE]:
            game_intro()
            Chosen.health = 100

        redraw_game_window()


game_intro()
game_loop()
pygame.quit()