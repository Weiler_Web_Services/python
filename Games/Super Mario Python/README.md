# Super Mario Implementation in Python


## Running

$ pip install pygame scipy
$ python main.py

## Standalone windows build

$ pip install py2exe
$ python compile.py py2exe

## Controls

* Left: Move left  
* Right: Move right  
* Space: Jump  
* Shift: Boost   
* Left/Right Mouseclick: secret   

## Current state:
![Alt text](img/pics.png "current state")

## Dependencies
* pygame
* scipy
