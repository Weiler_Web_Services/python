<h1><a href="https://github.com/siruidosp/URLExtractor">URLExtractor</a></h1>
<p>The extract urls of web server page</p>
<br>

<ul>
<li>Developer: Sir uidops</li>
<li>Email    : Sir.u1d0p5@gmail.com</li>
<li>Github   : https://github.com/siruidops/</li>
</ul>

## Dependencise 
## Installation [Linux](https://wikipedia.org/wiki/Linux) [![alt tag](http://icons.iconarchive.com/icons/dakirby309/simply-styled/32/OS-Linux-icon.png)](https://fr.wikipedia.org/wiki/Linux)

```bash
git clone https://github.com/siruidops/URLExtractor.git
cd URLExtractor
python install.py
python URLExtractor.py
```
## Installation [Android](https://wikipedia.org/wiki/Android) [![alt tag](https://cdn1.iconfinder.com/data/icons/logotypes/32/android-32.png)](https://fr.wikipedia.org/wiki/Android)

Download [Termux](https://play.google.com/store/apps/details?id=com.termux)

```bash
pkg update -y
pkg install python2 git -y
git clone https://github.com/siruidops/URLExtractor.git
cd URLExtractor
pip2 install -r requiremets.txt
python2 URLExtractor.py
```

## Installation [Windows ](https://wikipedia.org/wiki/Microsoft_Windows)[![alt tag](http://icons.iconarchive.com/icons/tatice/cristal-intense/32/Windows-icon.png)](https://fr.wikipedia.org/wiki/Microsoft_Windows)

```bash
Download Python2.7
Reboot the System
Download URLExtractor
Extract URLExtractor into Desktop
Open CMD and type the following commands:
cd Desktop/URLExtractor-master/
python Install.py
python URLExtractor.py
```

## Usage

<ul>
<li> python URLExtractor.py -u http://google.com</li>
<li> python URLExtractor.py -h</li>
</ul>

## 