#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Tested on windows, linux

# ---- ABOUT ----
# Developer: Sir u1d0p5
# Email    : Sir.u1d0p5@gmail.com
# Github   : https://github.com/siruidosp
# Telegram : https://t.me/sir_uidosp
# ---------------

# ---- OBJECTS ----
urlObject  = False
types = {'img':'src', 'a':'href', 'script':'src', 'link':'href'}
# -----------------


# ---- COLORS ----
red="\033[0;31m"
green="\033[0;32m"
brown="\033[0;33m"
pur="\033[0;35m" 
tur="\033[0;36m"
yellow="\033[1;33m"
end="\033[1;37m"
# ----------------

# ---- LIBRARY ----
import sys
import os
import urllib
import getopt

try:
	from bs4 import BeautifulSoup
except:
	print
	print red+" [-] Please Install 'bs4' Library"
	print green+"     pip install bs4"+end
	print
	sys.exit()
# --------------

# ---- LOGO ----
logo = yellow+"""
    _   _ ____  _     _____      _                  _
   | | | |  _ \| |   | ____|_  _| |_ _ __ __ _  ___| |_ ___  _ __
   | | | | |_) | |   |  _| \ \/ / __| '__/ _` |/ __| __/ _ \| '__|
   | |_| |  _ <| |___| |___ >  <| |_| | | (_| | (__| || (_) | |
    \___/|_| \_\_____|_____/_/\_ \\__|_|  \__,_|\___|\__\___/|_|
"""+end
# --------------

# ---- BANNER ----
def banner(Object):
	print
	print logo
	print
	print tur+"Developer"+end+": "+pur+"Sir Uidops"+end
	print tur+"Email    "+end+": "+pur+"Sir.u1d0p5@gmail.com"+end
	print tur+"Telegram "+end+": "+pur+"https://t.me/sir_uidops"+end
	print tur+"Github   "+end+": "+pur+"https://github.com/siruidops/URLExtractor/"+end
	print
	print yellow+"Switchs"+end+":"
	print 
	print tur+"  -u <URL>  "+end+": "+pur+"Url webServer for get urls"+end
	print tur+"  -h        "+end+": "+pur+"Show help banner"+end
	print
	print yellow+"Example"+end+":"
	print
	print brown+"  ./URLExtractor.py "+tur+"-u "+pur+"http://google.com/"+end
	print brown+"  ./URLExtractor.py "+tur+"-h"+end
	print
	sys.exit()
# ----------------

# ---- APPLICATION ----
def application(url):
	print
	print logo
	print
	try:
		html = urllib.urlopen(url).read()
	except:
		print red+"   [-] Not found your url: ",url,end
		print
		sys.exit()
	bs = BeautifulSoup(html, 'html.parser')
	for key,value in types:
		print brown+"   [!] Analyze urls of tag: "+key+end
		for i in bs.find_all(key):
			try:
				s = i[value]
				print green+"     ",s,end
			except:
				pass
# ---------------------

# ---- MAIN ----
def main():
	argv = sys.argv[1:]
	
	if len(sys.argv) == 1:
		banner()
	else:
		pass
	
	try:
		opts,args = getopt.getopt(argv, '-u:-h')
	except:
		banner()
	
	for opt,arg in opts:
		if opt == '-u':
			url = arg
		else:
			banner()
	
	try:
		application(url)
	except KeyboardInterrupt:
		print
		sys.exit()
# --------------

# ---- RUN APPLICATION ----
if __name__ == '__main__':
	main()
else:
	sys.exit()
# -------------------------







