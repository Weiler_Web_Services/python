Partial list of applications using Urwid

 * [alot](https://github.com/pazz/alot) Email client based on notmuch
   astronomical data analysis modules
 * [bpython](http://bpython-interpreter.org/) a fancy interface to the Python interpreter with experimental urwid display
 * [Check Commander](https://gitlab.com/larsfp/checkmk-commander) Checkmk client
 * [CHN](https://github.com/ghosthamlet/CHN) Hacker news on Console with auto classifer and recommender in reactjs style code
 * [d0xbase](https://sourceforge.net/projects/d0xbase/) Database about people, old-school style, like in the movies
 * [Debian's "reportbug" tool](http://packages.debian.org/stable/utils/reportbug)
 * [ECS Explorer](https://github.com/firemanphil/ecs_explorer) AWS EC2 Container Service navigator/explorer
   game development framework (discontinued)
 * [gertty](https://git.openstack.org/cgit/stackforge/gertty) Gerrit code review client
 * [Hachoir](https://github.com/vstinner/hachoir3) binary stream viewer/editor
 * [Isearch](https://github.com/steve-tyler/ISearch) Search Google, Bing and DuckDuckGo from the command line
 * [jmespath.terminal](https://github.com/jmespath/jmespath.terminal) JMESPath (JSON query language) exploration tool
 * [jsonwidget](http://blog.robla.net/2010/jsonwidget-python/) schema-based JSON editor
 * [lookatme](https://github.com/d0c-s4vage/lookatme) Terminal-based markdown presentation tool
 * [Lyvi](http://ok100.github.io/lyvi/) Command-line lyrics (and more!) viewer.
 * [Mercurial-easy](http://www.ivy.fr/mercurial/easy/) Console UI for Mercurial
 * [mitmproxy](http://mitmproxy.org/index.html) an SSL-capable man-in-the-middle proxy
 * [Mitzasql](https://vladbalmos.github.io/mitzasql/) - A MySQL text based client
 * [nooz](https://github.com/preetmishra/nooz) - Trending headlines right in your terminal
 * [nymp](https://github.com/thammi/nymp) an XMMS2 client with a tree view of your music collection
 * [oVirt Node](http://www.ovirt.org/Node) firmware-like slimmed down Fedora (configuration UI + installer are based on urwid)
 * [presentty](https://pypi.python.org/pypi/presentty) Console and RST based presentation software.
 * [PuDB](http://pypi.python.org/pypi/pudb) Python Urwid Debugger
 * [Pymissile](http://scott.weston.id.au/software/pymissile/) USB missile launch controller
 * [pyrasite-memory-viewer](https://github.com/lmacken/pyrasite/blob/develop/docs/MemoryViewer.rst) View the largest objects in your process
 * [Realtime GUI for the linux ipvs loadbalancer](http://sourceforge.net/projects/ipvsman/)
 * [redial](https://github.com/taypo/redial) SSH Session Manager
 * [rnr](https://github.com/bugnano/rnr) The RNR File Manager (RNR's Not Ranger)
 * [Salut à Toi](http://sat.goffi.org) (or SàT) XMPP (Jabber) client (ALPHA)
 * [sclack](https://github.com/haskellcamargo/sclack) Console Slack client
 * [scli](https://github.com/isamert/scli), unofficial [TUI](https://en.wikipedia.org/wiki/Text-based_user_interface) client for [Signal Private Messenger](https://github.com/signalapp/Signal-Android)
 * [sncli](https://github.com/insanum/sncli) Simplenote Command Line Interface
 * [Speedometer](http://excess.org/speedometer/) bandwidth monitor
 * [stig](https://github.com/rndusr/stig) TUI/CLI frontend for the Transmission BitTorrent client
 * [streamglob](https://github.com/tonycpsu/streamglob/) Console media browser with support for MLB.tv, NHL.tv, YouTube, Instagram, and more
 * [stsci_python](http://www.stsci.edu/resources/software_hardware/pyraf/stsci_python)
 * [subiquity](https://github.com/CanonicalLtd/subiquity) Ubuntu Server Installer
 * [s-tui](https://amanusk.github.io/s-tui/) s-tui Stress terminal UI CPU monitoring tool
 * [tv3](https://github.com/aramiscd/tv3) A text-based not-taking application (updated fork from [terminal_velocity](https://github.com/terminal-velocity-notes/terminal_velocity), a fast note-taking app for the UNIX terminal)
 * [Thousand Parsec](http://www.thousandparsec.net/tp/gettingstarted.php)
 * [todotxt-machine](https://pypi.python.org/pypi/todotxt-machine/) Interactive Todo.txt editor ([github page](https://github.com/AnthonyDiGirolamo/todotxt-machine))
 * [turses](https://github.com/alejandrogomez/turses) a Twitter client for the console
 * [upass](https://pypi.python.org/pypi/upass) urwid-based console interface for Password Store
 * [usolitaire](https://github.com/eliasdorneles/usolitaire) Solitaire clone for the console
 * [videotop](https://github.com/intnull/videotop) A console browser for online videos for websites like youtube
 * [VPN Gate with proxy](https://github.com/Dragon2fly/vpngate-with-proxy) Easy to use VPN Gate client (OpenVPN) under linux, also support for networks that are behind a proxy.
 * [Wicd](http://wicd.sourceforge.net) contains wicd-curses, an urwid-based interface
 * [wikicurses](https://github.com/ids1024/wikicurses) MediaWiki client
 * [XYZCommander](http://xyzcmd.syhpoon.name/) a visual shell/file manager (BETA)
 * [Zeitzono](https://zeitzono.org/) A city based timezone converter.
 * [Zulip-Terminal](https://github.com/zulip/zulip-terminal) Terminal interface for [Zulip](https://zulipchat.com)


Libraries using Urwid

 * [GViewer](https://github.com/chhsiao90/gviewer) urwid based TUI Library focus on log/report display
 * [panwid](http://github.com/tonycpsu/panwid) A collection of Urwid Widgets, including a data table and a dropdown list
 * [Sandook, ToDo list](https://github.com/ubhisat/Sandook)
 * [Urwid's Sàt Extensions](http://wiki.goffi.org/wiki/Urwid-satext) (urwid-satext) collection of widgets made for SàT project
 * [Urwid Tree Widgets](https://github.com/pazz/urwidtrees)

If you know of others, please add them to this page.