This is a list of some projects that we could really use a hand with. If you're interested, send an email to the mailing list or join us on IRC. We'd love to hear from you.

Non-coding projects:

* Contribute some Screen Shots of your favourite Urwid apps
* Blog about Urwid or projects using it

Beginner coding projects:

* Review and add doc strings to the source code
* Report bugs when you find them: IRC, mailing list, this ticket system are all fine options 
* Take apart and re-write some example programs (some of them are really showing their age)

Advanced coding projects:

* Write a display module for your favourite GUI toolkit (QT with PySide? would be great!)
* Read the Development wiki pages and join IRC to help out with new features
* Adopt the web_display module - something modern and websocket-y would be awesome
* Contribute some of your reusable widgets to the core library
* Package Urwid for your favourite distro 