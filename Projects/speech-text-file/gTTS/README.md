# gTTS

**gTTS** (*Google Text-to-Speech*), a Python library and CLI tool to interface with Google Translate's text-to-speech API.
Writes spoken `mp3` data to a file, a file-like object (bytestring) for further audio
manipulation, or `stdout`. <http://gtts.readthedocs.org/>

## Features

- Customizable speech-specific sentence tokenizer that allows for unlimited lengths of text to be read, all while keeping proper intonation, abbreviations, decimals and more;
- Customizable text pre-processors which can, for example, provide pronunciation corrections;
- Automatic retrieval of supported languages.

### Installation

    pip install gTTS

### Quickstart

Command Line:

    gtts-cli 'hello' --output hello.mp3

Module:

    >>> from gtts import gTTS
    >>> tts = gTTS('hello')
    >>> tts.save('hello.mp3')

See <http://gtts.readthedocs.org/> for documentation and examples.
