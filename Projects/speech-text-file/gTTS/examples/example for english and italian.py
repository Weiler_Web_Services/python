﻿#!/usr/bin/Python3

import os
from gtts import gTTS
from glob import glob

lst = []
lan = ["en", "it"]
for l in lan:
	with open("numbers.txt") as file:
		for n,line in enumerate(file):
			tts = gTTS(line, l)
			name = "number" + l + str(n) + ".mp3"
			tts.save(name)
			lst.append(name) # this is just to hear them with os....
			print(f"saved {name}")
[os.startfile(name) for name in lst]
