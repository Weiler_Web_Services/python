#!/bin/python3

import pyttsx3
from gtts import gTTS

engine = pyttsx3.init(driverName='sapi5')
infile = "some-file.txt"
f = open(infile, 'r')
theText = f.read()
f.close()

#Saving part starts from here
tts = gTTS(text=theText, lang='en')
tts.save("some-file.mp3")
print("File saved!")
