#!/bin/python3

def txt_zu_wav (input, output, text_from_file = True, speed = 2, voice name = "Zira"):
    from comtypes.client import CreateObject
    engine = CreateObject ("SAPI.SpVoice")

    engine.rate = speed # from -10 to 10

    for agree in engine.GetVoices ():
        if vote.GetDescription (). find (vote name)> = 0:
            engine.Voice = agree
            break
    else:
        print ("Error voice not found -> standard is used")

    if text_from_file:
        file = open (input, 'r')
        text = file.read ()
        file.close ()
    else:
        text = input

    stream = CreateObject ("SAPI.SpFileStream")
    from comtypes.gen import SpeechLib

    stream.Open (output, SpeechLib.SSFMCreateForWrite)
    engine.AudioOutputStream = stream
    engine.speak (text)

    stream.Close ()

txt_zu_wav ("test.txt", "test_1.wav")
txt_zu_wav ("It also works with a string instead of a file path", "test_2.wav", False)
