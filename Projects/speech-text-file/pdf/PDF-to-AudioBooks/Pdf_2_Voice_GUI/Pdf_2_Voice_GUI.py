#Importing Kivy packages
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.factory import Factory
from kivy.properties import ObjectProperty
from kivy.properties import BooleanProperty
from kivy.uix.popup import Popup
from kivy.core.audio import SoundLoader
from kivy.factory import Factory
from kivy.animation import Animation
from kivy.uix.progressbar import ProgressBar


#Importing Google text to speech
from gtts import gTTS

#Importing PDF reader PyPDF2
import PyPDF2

#Importing OS
import os

#Load Screen Class
class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)

#main Root Class
class Root(FloatLayout):
    isShownMenu = BooleanProperty(False)
    loadfile = ObjectProperty(None)
    
    # Dismiss popup method
    def dismiss_popup(self):
        self._popup.dismiss()

    #show load screen method
    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(1, 1))
        self._popup.open()

  
    #Load and convert pdf to mp3 method
    def load(self, path, filename):
        
      self.dismiss_popup()
      self._popup = Popup(title="Loading please wait",
                            size_hint=(1, 1))
      self._popup.open()
      
      #Conv to mp3 method
      def Conv():
    
        pdfReader = PyPDF2.PdfFileReader(filename[0])
        count = pdfReader.numPages
        TextList = []
        for i in range(count):
           try:
            page = pdfReader.getPage(i)
            print(page.extractText())
            TextList.append(page.extractText())
           except:
             pass

        TextString = " ".join(TextList)
        language = 'en'

        myobj = gTTS(text=TextString, lang=language, slow=False)

        myobj.save(filename[0]+"AudioBook.mp3")
        with open(os.path.join(path, filename[0])) as stream:
            self.text_input.text = TextString
        
      
        self.dismiss_popup()
      Conv()  


#Kivy file class
class PDF2MP3(App):
    pass


Factory.register('Root', cls=Root)
Factory.register('LoadDialog', cls=LoadDialog)



if __name__ == '__main__':
    PDF2MP3().run()