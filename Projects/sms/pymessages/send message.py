#!python3

# send message

from pymessages.client import MessagesClient

creds = MessagesClient.loadCredentialFile('credentials.json')
client = MessagesClient(creds)


TO = "+17174090085"
MSG = "Test message sent using PyMessages wrapper."

@client.on('authenticated')
async def onAuthenticated(service):
    print("Sending Messages.")
    await service.sendMessage(TO, MSG)
    print("Done.")

client.launch()
client.idle()
