# Game of Thrones Text Adventure in Python

## In Docker!

`$ docker build -t tyrion-game .`
`$ docker run -d -p 5000:5000 tyrion-game`

Visit http://localhost:5000 and play!
