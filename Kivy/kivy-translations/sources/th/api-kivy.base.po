# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2010, The Kivy Authors
# This file is distributed under the same license as the Kivy package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-10-04 17:12\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.9.0\n"

# 5f53c1f1e505465ca83bd98a1650c717
#: ../../sources/api-kivy.base.rst:3
msgid "Event loop management"
msgstr ""

# 723acd9f6f5d4d4fb0b5b633abe1b63a
#: ../../../kivy/base.py:docstring of kivy.base.EventLoop:1
msgid "EventLoop instance"
msgstr ""

# 385f266774da4424b205ffc301c71c6b
#: ../../<autodoc>:1
msgid "Bases: :class:`kivy.event.EventDispatcher`"
msgstr ""

# 2b08ddc7d0a44a30b98a6063e7fcf700
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase:1
msgid "Main event loop. This loop handle update of input + dispatch event"
msgstr ""

# 31f5caa6a8a74a1396bd2cacc1da87dd
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.add_event_listener:1
msgid "Add a new event listener for getting touch event"
msgstr ""

# 01e2df5290564d24bb8538ee3cfdc841
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.add_input_provider:1
msgid "Add a new input provider to listen for touch event"
msgstr ""

# 4dc44930a4ee4c35b786bb53addb911b
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.add_postproc_module:1
msgid "Add a postproc input module (DoubleTap, RetainTouch are default)"
msgstr ""

# a0c015f10fd54e7face2d6213ea09dd8
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.close:1
msgid "Exit from the main loop, and stop all configured input providers."
msgstr ""

# ad3ac619699b4b8d86e58d87f97bb4c0
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.dispatch_input:1
msgid "Called by idle() to read events from input providers, pass event to postproc, and dispatch final events."
msgstr ""

# ed642cf6acb544f89d01646346fbe69f
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.ensure_window:1
msgid "Ensure that we have an window"
msgstr ""

# e446318a2b204d879ce9254d509b850e
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.exit:1
msgid "Close the main loop, and close the window"
msgstr ""

# eb844bf582d84cb3a59bfa6cf91c5e31
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.idle:1
msgid "This function is called every frames. By default : * it \"tick\" the clock to the next frame * read all input and dispatch event * dispatch on_update + on_draw + on_flip on window"
msgstr ""

# d291c16324cf410f98057b50861268ee
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.on_pause:1
msgid "Event handler for on_pause, will be fired when the event loop is paused."
msgstr ""

# 3445991190624d0aad9057c66c60fa76
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.on_start:1
msgid "Event handler for on_start, will be fired right after all input providers have been started."
msgstr ""

# 85e020d09f234800a17086747e7356bb
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.on_stop:1
msgid "Event handler for on_stop, will be fired right after all input providers have been stopped."
msgstr ""

# fcf6689eaeef46578dfdd2f9f94db91f
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.post_dispatch_input:1
msgid "This function is called by dispatch_input() when we want to dispatch a input event. The event is dispatched into all listeners, and if grabbed, it's dispatched through grabbed widgets"
msgstr ""

# d5ff4d44a4a94f9dbc62a1aa27d6b8d0
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.remove_event_listener:1
msgid "Remove a event listener from the list"
msgstr ""

# 775f8c2c4fd445e3be534b840ddd65b7
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.remove_input_provider:1
msgid "Remove an input provider"
msgstr ""

# e8eec608c50949e4b215512a1f09869c
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.remove_postproc_module:1
msgid "Remove a postproc module"
msgstr ""

# d13e1e0889e04ab1948784eeb04702bd
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.run:1
msgid "Main loop"
msgstr ""

# 87e4c1f4c52c4fe58b18a6a3f33f6bfc
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.set_window:1
msgid "Set the window used for event loop"
msgstr ""

# edb13997992c42ec8038306c4cf18765
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.start:1
msgid "Must be call only one time before run(). This start all configured input providers."
msgstr ""

# 6214571c3e0d4ec5b39cb7ca09142d6c
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.stop:1
msgid "Stop all input providers and call callbacks registered using EventLoop.add_stop_callback()"
msgstr ""

# 401f8470c9c9410b81f6c168adbe013a
#: ../../../kivy/base.py:docstring of kivy.base.EventLoopBase.touches:1
msgid "Return the list of all touches currently in down or move state"
msgstr ""

# d63c1ce6d8d9400c90b9e586eacc10f4
#: ../../../kivy/base.py:docstring of kivy.base.ExceptionManager:1
msgid "Kivy Exception Manager instance"
msgstr ""

# 5cf56f176ee346378ca4a9567e81b81f
#: ../../../kivy/base.py:docstring of kivy.base.ExceptionHandler:1
msgid "Base handler that catch exception in runTouchApp(). You can derivate and use it like this ::"
msgstr ""

# 786cff3d88dd4719838565404afcb4c5
#: ../../../kivy/base.py:docstring of kivy.base.ExceptionHandler:11
msgid "All exceptions will be set to PASS, and logged to console !"
msgstr ""

# fdde4097c8d84ae3830e91df2a257b33
#: ../../../kivy/base.py:docstring of kivy.base.ExceptionHandler.handle_exception:1
msgid "Handle one exception, default return ExceptionManager.STOP"
msgstr ""

# 7bf3afc960a14dc390333a6f3adb7179
#: ../../../kivy/base.py:docstring of kivy.base.runTouchApp:1
msgid "Static main function that starts the application loop. You got some magic things, if you are using argument like this :"
msgstr ""

# f7b2e2909da24ff1b5ed7b4c005425e0
#: ../../../kivy/base.py:docstring of kivy.base.runTouchApp:6
msgid "To make dispatching work, you need at least one input listener. If not, application will leave. (MTWindow act as an input listener)"
msgstr ""

# c96043689e554ccdbe4f74660d1f6129
#: ../../../kivy/base.py:docstring of kivy.base.runTouchApp:11
msgid "If you pass only a widget, a MTWindow will be created, and your widget will be added on the window as the root widget."
msgstr ""

# b15c22eed89e4522a86732dc1391f412
#: ../../../kivy/base.py:docstring of kivy.base.runTouchApp:16
msgid "No event dispatching are done. This will be your job."
msgstr ""

# 8eb66c2993d747f0b422f6b47f76c2e7
#: ../../../kivy/base.py:docstring of kivy.base.runTouchApp:19
msgid "No event dispatching are done. This will be your job, but we are trying to get the window (must be created by you before), and add the widget on it. Very usefull for embedding Kivy in another toolkit. (like Qt, check kivy-designed)"
msgstr ""

# b33a50eee9c54582a934cf3df6f3b559
#: ../../../kivy/base.py:docstring of kivy.base.stopTouchApp:1
msgid "Stop the current application by leaving the main loop"
msgstr ""
