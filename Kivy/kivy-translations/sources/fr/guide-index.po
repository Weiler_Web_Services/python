# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2010, The Kivy Authors
# This file is distributed under the same license as the Kivy package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-10-04 16:54\n"
"PO-Revision-Date: 2011-10-04 17:07+0200\n"
"Last-Translator: Mathieu <mat@kivy.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Pootle 2.1.6\n"

# 673713ce1dc9473385ed15383ca5534c
#: ../../sources/guide-index.rst:2
msgid "Programming Guide"
msgstr "Guide de programmation"
