# Scenery-Generator

A Python Scenery Generator (images in the readme) using Python 3.8 and PyGame 2

<strong>Required</strong><br>
Python 2.7-3.8<br>
Pygame 2.0.0.dev6<br>

<img src="https://raw.githubusercontent.com/hamolicious/Scenery-Generator/master/Screenshots/picture1.PNG">

<img src="https://raw.githubusercontent.com/hamolicious/Scenery-Generator/master/Screenshots/picture2.PNG">

